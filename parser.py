#!/usr/bin/env python3

import csv
import datetime
import sys
from decimal import Decimal

current = Decimal(sys.argv[2])

def parse_time(s) -> str:
    return datetime.date(*reversed([int(e) for e in s.split('/')])).isoformat()

with open(sys.argv[1]) as csvf:
    c = csv.reader(csvf, delimiter=';')
    next(c)
    for row in c:
        if abs(Decimal(row[3])) > 100000: # ignore mortgage
            continue
        row.append(str(current))
        if row[1]:
            row[1] = parse_time(row[1])
        if row[2]:
            row[2] = parse_time(row[2])
        print ("\t".join(row))

        current -= Decimal(row[3])
